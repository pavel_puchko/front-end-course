var i = 0;
(function () {
	document.querySelector('#add').onclick = function (e) {
		i++;
		var toastText = i;
		var theDiv = document.createElement('div');
		theDiv.setAttribute("class","toast");
		theDiv.innerHTML = "<p>"+ toastText + " " + "здесь содержится текст оповещения" + "</p>";
		var that = document.querySelector('#toastsList');
		that.appendChild(theDiv);
		theDiv.addEventListener("webkitAnimationEnd", function(event) {
			var div = event.target;
			div.parentNode.removeChild(div);
		}
			, false);
	}
})()