'use strict';

var myApp = angular.module('myApp', ['ngRoute']);


myApp.service('Repositories', function($http) {
    this.getRepos = function(scope) { 
    	var myUrl = "https://api.github.com/search/repositories"
		myUrl += "?q=" + scope.query;
		myUrl += "+size:<=" + scope.maxRepoSize;
		myUrl += "+forks:>=" + scope.minForks;
		myUrl += "+stars:>=" + scope.minStars;
		myUrl += "&page=" + scope.page;
		myUrl += "&per_page=" + scope.perPage;
		myUrl += "&sort=full_name&order=asc";
		$http({ method: 'GET', url: myUrl }).success(function(data) {
			if (data.items.length < scope.perPage){
				scope.lastPage = true;
			}
			var repo = {};
			for(var i = 0; i < scope.perPage ; i++) {
				repo = {};
				repo.full_name = data.items[i].full_name;
				repo.owner = data.items[i].owner;
				repo.description = data.items[i].description;
				scope.repos.push(repo);
			}
		});
    };
});
myApp.controller('MainCtrl', ['$http', '$scope', 'Repositories', function ($http, $scope, Repositories) {
	$scope.query = "tetris";
	$scope.maxRepoSize = 20000;
	$scope.minForks = 5;
	$scope.minStars = 1;
	$scope.repos = [];
	$scope.page = 1;
	$scope.perPage = 10;
	$scope.lastPage = false;

		
	$scope.downloadRepos = function() {
		$scope.repos.length = 0;
		$scope.page = 1;
		$scope.lastPage = false;
		Repositories.getRepos($scope);
		/*function orderByFullName(a,b) {
			if (a.full_name < b.full_name)
				return -1;
			if (a.full_name > b.full_name)
				return 1;
			return 0;
		}

		$scope.repos.sort(orderByFullName);*/
	};
}]);
myApp.controller('DetailsCtrl', ['$http', '$scope', '$routeParams', function ($http, $scope, $routeParams) {
	$scope.id;
	$scope.description;
	$scope.avatar_url;
	$scope.login;
	$scope.full_name;
	$scope.contributors_url;
	$scope.contributors = [];
    var url = "https://api.github.com/repos/"+$routeParams.repoLogin + "/" + $routeParams.repoName;
    console.log(url);
	$http({ method: 'GET', url: url }).success(function(data) {
		$scope.full_name = data.full_name;
		$scope.id = data.owner.id;
		$scope.login = data.owner.login;
		$scope.avatar_url = data.owner.avatar_url;
		$scope.description = data.description;
		$scope.contributors_url = data.contributors_url;
			$http({ method: 'GET', url: $scope.contributors_url }).success(function(data) {
				var contrubutor = {};
				for(var i = 0; i < data.length ; i++) {
					contrubutor = data[i];
					$scope.contributors.push(contrubutor);
				}
			});
	});
}]);
myApp.directive('endlessScroll', function($window, $document, Repositories) {
	return {
		restrict: 'E',
		templateUrl : '../../views/scroll.html' ,
		link: function (scope, element, attrs) {
			angular.element($window).bind('scroll', function(e) {
				var scrollTop = angular.element($window).scrollTop();
				var scrollHeight = angular.element($document).height() - angular.element($window).height();
				if (scrollTop >= scrollHeight && !scope.lastPage) {
					scope.page++;
					Repositories.getRepos(scope);
				}

			})
		}
	}
});

myApp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: '../../views/main.html',
        controller: 'MainCtrl'
      }).
      when('/repositories/:repoLogin/:repoName', {
        templateUrl: '../../views/details.html',
        controller: 'DetailsCtrl'
      }).
      otherwise({
        redirectTo: '/'
	});
}]);

