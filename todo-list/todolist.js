(function () {
	var tasks = [];
	document.querySelector('#add').onclick = function (e) {
		var taskName = document.querySelector('#task').value;
		if (taskName !== '') {
		var theLi = document.createElement('li');
		theLi.innerHTML = "<div class='task_content'>"+taskName +"</div>" + "<div class='status'> In process </div>"+ "<br><div class='remove'> DELETE </div>";
		
		var that = document.querySelector('#task_list');
		that.appendChild(theLi);
		tasks.push({value: taskName, status: false});
		document.querySelector('#task').value = "";
		}
	}
	document.querySelector('#task_list').onclick = function (e) {
		var target = event.target;
		var parentElement = target.parentNode;
		var index = Array.prototype.indexOf.call(parentElement.parentNode.children, parentElement);
		if (target.classList.contains('remove'))
		{
			tasks.splice(index, 1);
			parentElement.parentNode.removeChild(parentElement);
		}
		if (target.classList.contains('status')) {
			if (tasks[index].status === false) {
				tasks[index].status = true;
				target.innerHTML = "COMPLETE";
				parentElement.style.margin="5px 40px 0";
				parentElement.style.border="3px solid green";
		    }
		    else 
		    {
		    	tasks[index].status = false;
		    	target.innerHTML = "In process";
		    	parentElement.style.margin="5px 0 0";
				parentElement.style.border="3px solid red";
		    }
			

		}
	}
})()