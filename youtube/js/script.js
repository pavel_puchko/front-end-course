var youtube = (function() {
    var layout = '<div><div class="header"><input class="search" id="search" type="text" placeholder="Введите запрос... Go" autofocus><img id="search_img" class="search-img" src="img/search.png"></div><div class="section" id="section"><ul class="content" id="content"></ul></div><div class="paging" id="paging"></div></div>';
    var limit = Math.floor(window.innerWidth / 330); // количество видяшек, которые влазят на экран
    var index = 1; //с какой видяшки посылаем запрос
    var load = 15; //сколько видяшек загружать при свайпе
    var loadCount = 0; //сколько видяшек ща загружено
    var pageCurrent = 1; //на какой странице мы находинмся
    var pageCount = 1; //сколько страниц всего
    var term; //тема запроса
    var styleTransform = document.createElement('style'); //для анимации

    document.body.innerHTML = layout;
    document.body.appendChild(styleTransform);

    var contentElement = document.querySelector('#content'); //наш элемент где видяшки
    var contentWindow = document.querySelector('#section'); //отоброжает нашу страницу
    var paging = document.querySelector('#paging'); //пейджинг
    var search = document.querySelector('#search'); // наша строка поиска

    var down = false; 
    var firstX = 0;
    var position = 0;
    var lastPosition = 0;
    var currentPosition =0;
    var newPosition = 0;

    var videoWidth = 340;
    var colorActivePage = "red";
    var colorPage = "white";

    paging.style.display = "none";
    contentWindow.style.display = "none";
    contentElement.style.webkitAnimationDuration = '0.5s'; //задаем время анимации
    contentElement.animationCount = 0;
    contentWindow.style.width = limit*videoWidth + 'px'; //устанавлием размер страницы
    paging.style.width = contentWindow.style.width;
    document.querySelector('body').style.background = "rgb(83,182,205)";
    document.querySelector('body').style.textAlign = "center";


    var generateName = (function () { //генератор уникальным имен функций
        var id = 0;

        return function () {
            id++;
            return 'callback' + id;
        }
    })();


    function jsonp(url, successCallback, errorCallback) { 
        var funcName = generateName();

        url += '&callback=' + encodeURIComponent(funcName);

        var script = document.createElement('script');
        script.src = url;
        script.async = true;

        // Success
        window[funcName] = function (data) {
            delete window[funcName];
            successCallback(data);
        };

        // Error
        script.onerror = function (err) {
            delete window[funcName];
            errorCallback(err);
        };

        document.body.appendChild(script);
    }

    function youtubeRequest(query, callback) {
        var url = 'http://gdata.youtube.com/feeds/api/videos/?v=2&alt=json&max-results=' + load +
            '&start-index=' + index + '&q=' + encodeURIComponent(query);


        var ul = document.querySelector('#content');

        var successCallback = function (data) {
            loadCount += load;
            data.feed.entry.forEach(function (video) {
                var divTitle = document.createElement('div');  //название видео вместе с ссылкой на ютуб
                divTitle.setAttribute('class', 'video-title');
                divTitle.innerHTML = "<a class='video-link' href='" + video.link[0].href +"'>"+ video.title.$t +"</a>";

                var img = document.createElement('img');  //картинка к видео
                img.setAttribute('class', 'video-img');
                img.setAttribute('src', video.media$group.media$thumbnail[1].url);

                var divDescription = document.createElement('div');  //описание к видео
                divDescription.setAttribute('class','video-description');
                divDescription.innerHTML = video.media$group.media$description.$t;

                var divViews = document.createElement('div'); //просмотры
                divViews.setAttribute('class', 'video-views');
                divViews.innerHTML = "Views: " + video.yt$statistics.viewCount;

                var divAuthor = document.createElement('div');  //имя автора
                divAuthor.setAttribute('class', 'video-author');
                divAuthor.innerHTML = "Author: " + video.author[0].name.$t;

                var date = new Date(Date.parse(video.updated.$t));  //дата видео
                var divDate = document.createElement('div');
                divDate.setAttribute('class', 'video-date');
                divDate.innerHTML = "Date: " + date.toUTCString();


                var li = document.createElement('li');
                li.setAttribute('class', 'video')

                li.appendChild(divTitle);
                li.appendChild(divDate);
                li.appendChild(divAuthor);
                li.appendChild(divViews);
                li.appendChild(img);
                li.appendChild(divDescription);
                ul.appendChild(li);
            });
            createPaging();
            if (callback) {
                callback();
            }
        };

        var errorCallback = function (err) {
            document.body.innerText = "Ooops, Panic!";
            console.error(err);
        };

        jsonp(url, successCallback, errorCallback);
    }


    window.onresize = function () {
        limit = Math.floor(window.innerWidth / videoWidth); //пересчитыываем сколько влазит видяшек
        contentWindow.style.width = limit*videoWidth + 'px'; //устанавлием новый размер страницы
        pageCount = Math.ceil(loadCount / limit); //пересчитываем количество страниц
        pageCurrent = Math.floor(newPosition / (limit * videoWidth) * -1 + 1); //пересчитвыем текущую страницу
        paging.style.width = contentWindow.style.width;
        createPaging(); //меняем пейджинг
        changeActivePage(pageCurrent);//меняем текущую страницу
    }

    contentElement.addEventListener('mousedown', function (event) { //начало свайпа только на контенте
        down = true;
        firstX = event.x;
    });
   contentElement.addEventListener('touchstart', function (event) { //начало свайпа только на контенте
        down = true;
        firstX = event.changedTouches[0].pageX;
        contentElement.focus();
    });
    document.body.addEventListener('mouseup', function (event) { //продолжать свайп можно вне контента
        var shift;
        if (down) {
            down = false;
            shift = lastPosition - currentPosition; //определяем сдвиг
            if (shift <= -75) {
                if (pageCurrent > 1){
                    pageCurrent--;
                }
            }
            if (shift >= 75) {
                pageCurrent++;
            }
            else {
                newPosition = lastPosition;
            }
            if (newPosition > 0){
                newPosition = 0;
            }
            changePage(pageCurrent);
        }
    });
    document.body.addEventListener('touchend', function (event) { //продолжать свайп можно вне контента
        var shift;
        if (down) {
            down = false;
            shift = lastPosition - currentPosition; //определяем сдвиг
            if (shift <= -100) {
                if (pageCurrent > 1){
                    pageCurrent--;
                }
            }
            if (shift >= 100) {
                pageCurrent++;
            }
            else {
                newPosition = lastPosition;
            }
            if (newPosition > 0){
                newPosition = 0;
            }
            changePage(pageCurrent);
        }
    });
    document.body.addEventListener('mousemove', function (event) {
        if (down) {
            position = event.x - firstX;
            currentPosition = position + lastPosition;
            contentElement.style.webkitTransform = 'translate3d('+currentPosition+'px, 0, 0)';
        } 
    });
    document.body.addEventListener('touchmove', function (event) {
        if (down) {
            position = event.changedTouches[0].pageX - firstX;
            currentPosition = position + lastPosition;
            contentElement.style.webkitTransform = 'translate3d('+currentPosition+'px, 0, 0)';
        } 
    });

    var changePage = function(page) {
        if (page !== 1)
            newPosition = ((page-1) * limit*videoWidth) * -1;
        else 
            newPosition = 0;
        var animationFunction = '@-webkit-keyframes slide' +contentElement.animationCount+ '{from{-webkit-transform: translate(' + currentPosition + 'px);}to{-webkit-transform: translate3d(' + newPosition + 'px, 0, 0);}}';
        contentElement.style.webkitAnimationName = 'slide' + contentElement.animationCount;
        contentElement.animationCount++;
        styleTransform.innerHTML = animationFunction;
        contentElement.style.webkitTransform = 'translate3d('+newPosition+'px, 0, 0)';
        lastPosition = newPosition;
        currentPosition = newPosition;
        changeActivePage(pageCurrent);
        if (pageCount - pageCurrent <= 3) { //подгружать новые видео только если осталось <= 3 страниц
            index += load;
            youtubeRequest(term);
        }

    };

    document.querySelector('#search_img').addEventListener('click', function(e) {
        if (search.value !== ""){
            setDefault();
            paging.style.display = "block";
            contentWindow.style.display = "block";
            term = search.value;
            document.querySelector('#content').focus();
            youtubeRequest(term);
        }
    });
    search.addEventListener('keypress', function(e) {
        if (e.keyCode === 13) {
            if (search.value !== ""){
                setDefault();
                paging.style.display = "block";
                contentWindow.style.display = "block";
                term = search.value;
                document.querySelector('#content').focus();
                youtubeRequest(term);
            }
        }
    });

    var setDefault = function() {
        contentElement.innerHTML = "";
        contentElement.style.webkitTransform = 'translate3d(0, 0, 0)'; 
        down = false;
        firstX = 0;
        position = 0;
        lastPosition = 0;
        newPosition = 0;
        index = 1;
        loadCount = 0;
        pageCount = 1;
        pageCurrent = 1;
    }
    var changeActivePage = function(page) {
        var i;
        for (i = 1; i < pageCount + 1; i++) {
            if (i == page){
                document.querySelector('#page' + i).style.background = colorActivePage;
            }
            else {
                document.querySelector('#page' + i).style.background = colorPage;          
            }
        }
    }
    var createPaging = function() {
        var i;
        pageCount = Math.ceil(loadCount / limit);
        paging.innerHTML = "";
        for (i = 1; i < pageCount + 1; i++) {
                var divPage = document.createElement('div');  
                divPage.setAttribute('class', 'page'); 
                divPage.setAttribute('id', 'page' + i); 
                divPage.innerHTML = i;
                divPage.addEventListener('click', function (event) {
                  pageCurrent = event.target.id.substring(4);
                  changePage(pageCurrent);
                  changeActivePage(pageCurrent);
                });
                document.querySelector('#paging').appendChild(divPage);
            }
        changeActivePage(pageCurrent);
    }
})();