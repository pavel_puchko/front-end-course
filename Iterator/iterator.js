function iterator(inputArray, width, cyclic, func) { //обязательный параметр только массив
	var _this = this;
	this.array = inputArray;
	this.cyclic = cyclic || false;
	this.func = func || undefined;
	this.params = {
		startPosition : 0,
		endPosition : (width < inputArray.length-1 && width > 0) ? width : (typeof width !== "undefined") ? 0 : 1 //если ширина не влазит в массив, то окно -весь массив, если не определена, то окно = 1
	}
	Array.observe(this.array, function(changes){   //добавляем реаакцию итератора на изменение массива
		changes.forEach(function(change) {
		if (change.type == "splice") {
			var deltaWidth = change.addedCount - change.removed.length;
			if (deltaWidth != 0 && change.index <= _this.params.startPosition) {
				if (_this.params.startPosition >= _this.params.endPosition || _this.params.endPosition == 0) {
				_this.moveWindow(deltaWidth, 0);
				}
				else {
					_this.moveWindow(deltaWidth,deltaWidth);
				}
			}
		}
		});
	});
}

iterator.prototype.printWindow = function(inputArray, startPosition, endPosition) {   //выводит окно, минимум 1 элемент
	var array = inputArray, 
		stPos = startPosition,
		endPos = endPosition, 
		curPos = (stPos + 1) % array.length;
		result = [];
	if (this.array.length !== 0) { //если наш массив, то возвращаем пустое окно, иначе
		result.push(array[stPos]); //всегда возвращать минимум 1 элемент
		while (curPos !== endPos) {
			result.push(array[curPos]);
			curPos = (curPos + 1) % array.length;
		}
	}
	return result;
}

iterator.prototype.correctParamsAndCallFunction = function(params, isNext) { //корректировка параметров после вызова функции
	var newParams = params;
	if (newParams.endPosition === 0) { //для того чтобы функция могла сузить окно 
		newParams.endPosition = this.array.length;
	}
	newParams = this.func(newParams, isNext) // вызов функции, передаем объект параметров и флаг некст/прев
	if (this.cyclic === false) {
		if ((newParams.startPosition < 0) || (newParams.startPosition > this.array.length-1)) {
			newParams.startPosition = 0;
		}
		if ((newParams.endPosition < newParams.startPosition) && (newParams.endPosition !== 0)){
			newParams.endPosition = newParams.startPosition + 1;
		}
		if (newParams.endPosition < 0) {
			newParams.endPosition = (this.array.length + this.params.endPosition) % this.array.length;
		}
		if ((newParams.endPosition > this.array.length-1)){
			newParams.endPosition = 0;
		}
	}
	else {
		newParams.startPosition = (this.array.length + newParams.startPosition ) % this.array.length;
		newParams.endPosition = (this.array.length + newParams.endPosition ) % this.array.length;
	}
	return newParams;
}

iterator.prototype.next = function() {
	if (typeof this.func !== "undefined"){ //вызываем функцию только если она объявлена
		this.params = this.correctParamsAndCallFunction(this.params, true);
	}
	this.moveWindow(1,1);
	return this.printWindow(this.array, this.params.startPosition, this.params.endPosition);
}

iterator.prototype.prev = function() {
	if (typeof this.func !== "undefined"){ //вызываем функцию только если она объявлена
		this.params = this.correctParamsAndCallFunction(this.params, false);
	}
	this.moveWindow(-1,-1);
	return this.printWindow(this.array, this.params.startPosition, this.params.endPosition);
}

iterator.prototype.move = function(stPos,endPos) { //функция двигающая окно на заданные индексы(могут быть отрицательными)
	this.params.startPosition = (this.array.length + this.params.startPosition + stPos) % this.array.length;
	this.params.endPosition = (this.array.length + this.params.endPosition + endPos) % this.array.length;
}

iterator.prototype.moveWindow = function(stPos, endPos) { //функция определяющая можно ли двигать окно, если да, то двигаем
	if (isNaN(this.params.startPosition) || isNaN(this.params.endPosition)){ //если полностью удалить массив, а потом добавнить элементы, то окно = 1 
		this.params.startPosition = 0;
		this.params.endPosition = 1;
	}
	if ((this.cyclic === false) && (endPos > 0)){ //без цикла, определяем направление движения окна
		if (this.params.endPosition !== 0) {
			this.move(stPos,endPos);
		}
	}
	if ((this.cyclic === false) && (stPos < 0)){ //без цикла, определяем направление движения окна
		if (this.params.startPosition !== 0) {
			this.move(stPos,endPos);
		}
	}
	if (this.cyclic) {  //для циклического 
		this.move(stPos,endPos);
	}	
}
function funct(params, isNext) { //вызвваемся функция
	var newParams = params;
	if (isNext){
		newParams.endPosition++;
	}
	else{
		newParams.startPosition -= 2;
		newParams.endPosition -= 2;
	}
	return newParams;
}