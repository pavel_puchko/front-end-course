module.exports = function(grunt) {
		grunt.initConfig({
			pkg: grunt.file.readJSON('package.json'),
			uglify: {
				options:{ 
					banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' + '<%= grunt.template.today("yyyy-mm-dd") %> */'
				},
				generated: {
					files: {
						'build/output.min.js':['js/**/*.js'],
						'build/output.libs.min.js':['bower_components/jquery/jquery.js','bower_components/underscore/underscore.js','bower_components/backbone/backbone.js','bower_components/backbone.localStorage/backbone.localStorage-min.js','bower_components/backbone.localStorage/todos.js','bower_components/todomvc-common/base.js']
					}
				}
			},
			jshint: {
			    generated: ['js/**/*.js']
  			},
  			cssmin: {
				  generated: {
				    options: {
				      banner: '/* My minified css file */'
				    },
				    files: {
				      'build/output.min.css': ['bower_components/todomvc-common/base.css']
				    }
				  }
			},
			usemin: {
				html: 'index.html'
			}
		});

		grunt.loadNpmTasks('grunt-usemin');
		grunt.loadNpmTasks('grunt-contrib-uglify');
		grunt.loadNpmTasks('grunt-contrib-jshint');
		grunt.loadNpmTasks('grunt-contrib-cssmin');
  		grunt.registerTask('default', ['jshint:generated','cssmin:generated','uglify:generated', 'usemin']);
};