var Puzzle = function(vol1, vol2 ,req_measure) { //передаем емкость первого стакана, второго, и условие победы
	this.tub_1 = { 
    max_size: vol1, 
    cur_size : 0
  },
  this.tub_2 = {
    max_size: vol2,
    cur_size: 0
  },
  this.victory = req_measure;
};
Puzzle.prototype.pour = function(tub1, tub2) {  //переливаем воду из первого стакана во второй
	while (tub1.cur_size !== 0) {
    if (tub2.cur_size !== tub2.max_size) {
      tub1.cur_size--;
      tub2.cur_size++;
    }
    else 
      break;
  }
  if (tub1.cur_size === this.victory || tub2.cur_size === this.victory) { //если где-то окзалось количество литров равных победным, то это вин
    tub1.cur_size = 0;
    tub2.cur_size = 0;
    return "Good Job";
  }
  return [tub1, tub2];
};
Puzzle.prototype.fill = function(tub) { //полностью заполнить стакан водой
	tub.cur_size = tub.max_size;
  return tub;
};
Puzzle.prototype.out = function(tub) { //вылить воду из стакана
  tub.cur_size = 0;
  return tub;
}