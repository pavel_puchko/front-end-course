var Puzzle = function() {
	function pour() {
		return 0;
	},

	function fill(){
		return 0;
	}
};

var assert = chai.assert;
describe('Puzzle', function(){
  describe('#pour()', function(){
    it('should return new tubs', function(){
      var puzzle = new Puzzle(4,3,2);
      assert.deepEqual([{size: 4, cur_size: 1}, {size: 3, cur_size: 3}], puzzle.pour({size: 4, cur_size: 4},{size: 3, cur_size: 0}));
    });
  });
    describe('#fill()', function(){
    it('should return full tub', function(){
      var puzzle = new Puzzle(4,3,2);
      assert.deepEqual({size: 4, cur_size: 4}, puzzle.fill({size: 4, cur_size: 2});
    });
  });
});