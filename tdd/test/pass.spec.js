var assert = chai.assert;
var puzzle = new Puzzle(4,3,2);
describe('Puzzle', function(){
  describe('#pour()', function(){
    it('should return new correct tubs', function(){
      assert.deepEqual([{max_size: 4, cur_size: 1}, {max_size: 3, cur_size: 3}], puzzle.pour({max_size: 4, cur_size: 4},{max_size: 3, cur_size: 0}));
      assert.deepEqual([{max_size: 4, cur_size: 0}, {max_size: 3, cur_size: 0}], puzzle.pour({max_size: 4, cur_size: 0},{max_size: 3, cur_size: 0}));

    });
    it('should return victory', function(){
      assert.equal("Good Job", puzzle.pour({max_size:4, cur_size: 4}, {max_size: 3, cur_size: 1}));
      assert.equal("Good Job", puzzle.pour({max_size:3, cur_size: 1}, {max_size: 4, cur_size: 1}));

    });
  });
    describe('#fill()', function(){
    it('should return full tub', function(){
      assert.deepEqual({max_size: 4, cur_size: 4}, puzzle.fill({max_size: 4, cur_size: 2}));
      assert.deepEqual({max_size: 4, cur_size: 4}, puzzle.fill({max_size: 4, cur_size: 0}));

    });
  });
     describe('#out()', function(){
    it('should return empty tub', function(){
      assert.deepEqual({max_size: 4, cur_size: 0}, puzzle.out({max_size: 4, cur_size: 2}));
      assert.deepEqual({max_size: 4, cur_size: 0}, puzzle.out({max_size: 4, cur_size: 4}));

    });
  });
});