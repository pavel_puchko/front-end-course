function make() {
    var args = [].slice.call(arguments),
        func = args[args.length - 1];
 
    if (typeof func === 'function') {
        return [].slice.call(arguments, 0, arguments.length - 1).reduce(func);
    }
 
    return make.bind.apply(make, [null].concat(args));
}