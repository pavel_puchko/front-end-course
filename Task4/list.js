/*ИНДЕКСАЦИЯ С 1 */

function DLList() {
	this.head_list = null;
	this.tail_list = null;
	this.countElements = 0; /*ИНДЕКСАЦИЯ С 1 */
}

DLList.prototype.head = function () { //Работает
		return this.head_list;
}
DLList.prototype.tail = function () {  //работает
		return this.tail_list;
}

DLList.prototype.append = function (val) {  //работает
	if (arguments.length < 1 )
		throw new Error("Bad arguments");
	if (!this.head_list) {
		this.head_list = {value: val, prev: null, next: null};
		this.tail_list = this.head_list;
	}
	else {
		this.tail_list.next = {value: val, prev: this.tail_list, next: null};
		this.tail_list = this.tail_list.next;
	}
	this.countElements++;
	return this;
}

DLList.prototype.at = function (index) {  //работает
	var i, current = this.head_list;
	if (typeof(index) !== "number") {
		throw new Error("Bad index");
	}
	if (arguments.length < 1 )
		throw new Error("Bad arguments");
	if (this.head_list == null)
		throw new Error("List empty");
	if (index <= 0 || index > this.countElements)
		throw new Error("Index out of range");
	for (i = 1; i <= index-1; i++){
		current = current.next;
	}
	return current;
}

DLList.prototype.insertAt = function (index, val) { //работает
	var i, current = this.head_list, newElement
	if (arguments.length < 2 )
		throw new Error("Bad arguments");
	if (typeof(index) !== "number") 
		throw new Error("Bad index");
	if (this.head_list == null)
		throw new Error("List empty");
	if (index <= 0 || index > this.countElements)
		throw new Error("Index out of range");
	for (i = 1; i <= index-1; i++){
		current = current.next;
	}
	if (index === 1) { //вставляем элемент на место head
		newElement = {value: val , prev: null, next: this.head_list};
		this.head_list.prev = newElement;
		this.head_list = newElement;
		this.countElements++;
		return this;
	}
	newElement = {value: val, prev: current.prev, next: current};
	current.prev.next = newElement;
	current.prev = newElement;	
	this.countElements++;
	return this;
}

DLList.prototype.reverse = function() { //работает
    var current = newTail = this.head_list;
    var newHead = null;
    while (current) {
        var temp = current.next;
        current.next = newHead;
        current.prev = temp;
        newHead = current;
        current = temp;
    }
    this.head_list = newHead;
    this.tail_list = newTail;
    return this;
}

DLList.prototype.deleteAt = function (index) { // работает
	var i, current = this.head_list, previous;
	if (arguments.length < 1 )
		throw new Error("Bad arguments");
	if (typeof(index) !== "number") 
		throw new Error("Bad index");
	if (this.head_list == null)
		throw new Error("List empty");
	if (index <= 0 || index > this.countElements)
		throw new Error("Index out of range");
	if (index === 1) { //удаляем голову
		this.head_list = this.head_list.next;
		this.head_list.prev = null;
		this.countElements--;
		return this;
	}
	if (index === this.countElements) {  //удаляем хвост
		this.tail_list = this.tail_list.prev;
		this.tail_list.next = null;
		this.countElements--;
		return this;
	}
	for (i = 1; i <= index-1; i++){ //остальные элементы
		current = current.next;
	}
	previous = current.prev;
	previous.next = current.next;
	current.next.prev = previous;
	return this;
}

DLList.prototype.each = function (funct) { //работает
	var current = this.head_list;
	if (arguments.length < 1 )
		throw new Error("Bad arguments");
	while (current) {
		current.value = funct(current.value);
		current = current.next;
	}
	return this;
}

var funct = function (value) {  //работает (функия для метода each)
	value = value*value;
	return value;
}

DLList.prototype.indexOF = function (val) {  // работает
	var i, current = this.head_list, index;
	if (this.head_list == null)
		throw new Error("List empty");
	for (i = 1; i <= this.countElements; i++){
		if (current.value === val){ 
			index = i;
			return index;
		}
		current = current.next;
	}
	return null;
}

DLList.prototype.print = function() {  // Работает (Выводит все элемента списка)
	var current = this.head_list;
	while (current) {
		console.log(current.value);
		current = current.next;
	}
	return this;
}

DLList.prototype.go = function(count) {  // Заполнение списка для теста
	var i;
	for (i = 1; i <= count; i++)
		this.append(i);
	return this;
}